gkermit (2.01-4) unstable; urgency=medium

  * QA upload.
  * Fix building with -Werror=implicit-function-declaration.
    (Closes: #1066387)
  * Declare Rules-Requires-Root: no.
  * Enable all hardening flags.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 05 Apr 2024 03:32:43 +0200

gkermit (2.01-3) unstable; urgency=low

  * Orphan package - see bug 1034447.

 -- Bastian Germann <bage@debian.org>  Tue, 18 Apr 2023 16:22:28 -0000

gkermit (2.01-2) unstable; urgency=medium

  * Switch to dh

 -- Bastian Germann <bage@debian.org>  Sat, 03 Dec 2022 18:08:15 +0100

gkermit (2.01-1) unstable; urgency=medium

  * New upstream version 2.01
  * Drop unneeded patch

 -- Bastian Germann <bage@debian.org>  Fri, 25 Mar 2022 22:29:13 +0100

gkermit (2.00-1) unstable; urgency=medium

  [ Justin B Rye ]
  * Rephrase cobwebby package description (Closes: #701604, #732938)

  [ Helmut Grohne ]
  * Fix FTCBFS: (Closes: #909129)
    + Let dh_auto_build pass cross tools to make.
    + Build the gwart tool for the build architecture.

  [ Bastian Germann ]
  * Change to new homepage
  * New upstream version 2.00
  * Rebase the patches
  * Adopt the package (Closes: #836483)
  * Make the package build with buildflags
  * Raise debhelper-compat version to v13

 -- Bastian Germann <bage@debian.org>  Fri, 15 Oct 2021 22:18:14 +0200

gkermit (1.0-11) unstable; urgency=medium

  * QA upload
  * Remove dpatch and various lintian cleanups, thanks to Jari Aalto
    (Closes: #669657)
  * Switch to debhelper-compat (= 10)

 -- Moritz Muehlenhoff <jmm@debian.org>  Mon, 22 Jul 2019 19:29:21 +0200

gkermit (1.0-10) unstable; urgency=medium

  * QA upload
  * Set maintainer field to Debian QA Group (see #836483)
  * Bump debhelper compat level. Closes: #817475

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sun, 18 Dec 2016 14:53:05 +0100

gkermit (1.0-9) unstable; urgency=low

  * Bumped to Standards-Version: 3.8.0.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Tue, 15 Jul 2008 10:50:48 +0900

gkermit (1.0-8) unstable; urgency=low

  * debian/doc-base: Removed an extra whitespace (to shut lintian up).

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Sat, 08 Dec 2007 22:20:52 +0900

gkermit (1.0-7) unstable; urgency=low

  * Bumped to Standards-Version: 3.7.3.
  * Fixed various lintian warnings.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Fri, 07 Dec 2007 09:33:46 +0900

gkermit (1.0-6) unstable; urgency=low

  * Fixed 100% CPU usage problem during send, patch kindly provided by Steve Fosdick - closes: #409182

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Tue, 01 May 2007 11:48:53 +0900

gkermit (1.0-5) unstable; urgency=low

  * Bumped to Standards-Version: 3.7.2.
  * Added progress indicator, thanks Tim Riker - closes: #266953

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Fri, 13 Oct 2006 10:25:00 +0900

gkermit (1.0-4) unstable; urgency=low

  * [control] changed Maintainer field.
  * Bumped to Standards-Version: 3.6.1.

 -- Masayuki Hatta (mhatta) <mhatta@debian.org>  Sat, 14 Feb 2004 18:48:10 +0900

gkermit (1.0-3) unstable; urgency=low

  * New Maintainer - closes: #143652
  * Removed README.Debian.
  * Fixed typo in description.
  * Revised copyright.

 -- Masayuki Hatta <mhatta@debian.org>  Thu,  8 Aug 2002 15:04:27 +0900

gkermit (1.0-2) unstable; urgency=low

  * Package is orphaned (see #143652); maintainer set to Debian QA Group.
  * Fix typo in description (closes: #124673).
  * Add some missing #include directives (closes: #112858).
  * Don't install upstream README file as it is already installed as
    gkermit.txt.
  * Upgrade to debhelper v4.
  * Bump Standards-Version to 3.5.6.
  * Support DEB_BUILD_OPTIONS.
  * Add Build-Depends on debhelper.
  * Remove emacs settings from bottom of this changelog.
  * Added a doc-base file.

 -- Robert Luberda <robert@debian.org>  Mon, 10 Jun 2002 23:16:53 +0200

gkermit (1.0-1) unstable; urgency=low

  * Initial Release.

 -- Vaidhyanathan G Mayilrangam <vaidhy@debian.org>  Tue,  4 Jan 2000 14:13:19 -0500
